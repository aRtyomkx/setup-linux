FROM archlinux:latest

# ---
# Packages
RUN pacman -Syu --noconfirm
RUN pacman-key --init
RUN pacman -Syu --noconfirm \
    neovim \
    tmux \
    npm \
    zig \
    rust \
    ripgrep \
    bottom \
    lazygit
# ----


# ---
# User
RUN useradd -m -G wheel -s /bin/bash artk
# Develop permission
RUN mkdir -p /home/artk/develop
RUN chown artk /home/artk/develop
USER artk
WORKDIR /home/artk
# ---

# ----
# ohmybash
RUN bash -c "$(curl -fsSL https://raw.githubusercontent.com/ohmybash/oh-my-bash/master/tools/install.sh)"
# ----


# ----
# TMUX
RUN git clone https://gitlab.com/aRtyomkx/conf-tmux.git \
  && chmod 777 ./conf-tmux/install.sh \ 
  && ./conf-tmux/install.sh \
  && rm -rf ./conf-tmux

ENV TERM=xterm-256color
# ----

# ----
# neovim
RUN git clone https://gitlab.com/aRtyomkx/conf-neovim.git "${XDG_CONFIG_HOME:-$HOME/.config}"/nvim
RUN nvim --headless "+Lazy! sync" +qa
# ----

