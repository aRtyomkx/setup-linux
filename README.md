# config-ae

My own quick docker dev workflow
## Docker Compose (RECOMENDED)

Base folder is `/home/artk` and with machine volume is `/home/artk/develop` attached to docker develop volume

1. Create volume
```bash
docker volume create develop
```

1. Create network
```bash
docker network create develop
```

1. Build and run
```bash
docker compose up -d
```

1. Exec
```bash
docker exec -it develop tmux new-session -A -s base
```

## Install
1. Build base image
```bash
docker build -t local/dev .
```

1. Run base in background
```bash
docker run -d local/dev sleep infinity
```

1. Create terminal
```bash
docker run -it local/dev /bin/bash
```


